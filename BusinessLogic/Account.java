package BusinessLogic;

public class Account {
	
	String accountID;
	String type;
	float amount;
	String dateofCreation;
	String clientID;
	public Account(String accountID, String type, float amount, String dateofCreation, String clientID) {
		super();
		this.accountID = accountID;
		this.type = type;
		this.amount = amount;
		this.dateofCreation = dateofCreation;
		this.clientID = clientID;
	}
	public Account() {
		// TODO Auto-generated constructor stub
	}
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getDateofCreation() {
		return dateofCreation;
	}
	public void setDateofCreation(String dateofCreation) {
		this.dateofCreation = dateofCreation;
	}
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	

}
