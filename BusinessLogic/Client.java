package BusinessLogic;

public class Client {
	
	String clientID;
	String name;
	String IDnumber;
	String CNP;
	String address;
	String loginID;
	public Client(String clientID, String name, String iDnumber, String cNP, String address, String loginID) {
		super();
		this.clientID = clientID;
		this.name = name;
		IDnumber = iDnumber;
		CNP = cNP;
		this.address = address;
		this.loginID = loginID;
	}
	public Client() {
		// TODO Auto-generated constructor stub
	}
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIDnumber() {
		return IDnumber;
	}
	public void setIDnumber(String iDnumber) {
		IDnumber = iDnumber;
	}
	public String getCNP() {
		return CNP;
	}
	public void setCNP(String cNP) {
		CNP = cNP;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	
	

}
