package BusinessLogic;

public class Login {
	
	String loginID;
	String username;
	String password;
	boolean role;
	public Login(String loginID, String username, String password, boolean role) {
		super();
		this.loginID = loginID;
		this.username = username;
		this.password = password;
		this.role = role;
	}
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isRole() {
		return role;
	}
	public void setRole(boolean role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "Login [loginID=" + loginID + ", username=" + username + ", password=" + password + ", role=" + role
				+ "]";
	}
	
	

}
