package DatabaseAccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import BusinessLogic.Account;

public class AccountMapper {
	
	public static void insertAccount(Account a) throws SQLException {
		String statement= "INSERT INTO Account (accountID, type, amount, dateofCreation, clientID) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement prepSt = DBConnection.getConnection().prepareStatement(statement);
		prepSt.setString(1, a.getAccountID());
		prepSt.setString(2, a.getType());
		prepSt.setFloat(3, a.getAmount());
		prepSt.setString(4, a.getDateofCreation());
		prepSt.setString(5, a.getClientID());
		
		int succes= prepSt.executeUpdate();
		if(succes == 1)
			System.out.println("Account inserted!");
	}
	
	public Account getAccountById(String accountID) throws SQLException {
		Account foundAccount= new Account();
		String sqlQuery="SELECT accountID, type, amount, dateofCreation, clientID FROM Account";
		PreparedStatement prepSt= DBConnection.getConnection().prepareStatement(sqlQuery);
		prepSt.setString(100, accountID);
		ResultSet queryResult=prepSt.executeQuery();
		
		while (queryResult.next()) {
			String id=queryResult.getString("accountID");
			String type=queryResult.getString("type");
			float amount=queryResult.getFloat("amount");
			String dateofCreation=queryResult.getString("dateofCreation");
			String clientID=queryResult.getString("clientID");
			
			foundAccount.setAccountID(id);
			foundAccount.setType(type);
			foundAccount.setAmount(amount);
			foundAccount.setDateofCreation(dateofCreation);
			foundAccount.setClientID(clientID);
			
			System.out.println("Account ID: "+id+", Type: "+type+", Amount: "+amount+", Date of creation:"+dateofCreation+", clientID:"+clientID);
		}
		return foundAccount;
	}
	
	public static void deleteAccount(String idToDelete) throws SQLException{
		
		
		try {
		String statement= "DELETE from Account where accountID=?";
		PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
		prepSt.setString(1, idToDelete);


		int succes = prepSt.executeUpdate();
		
		if(succes == 1)
			System.out.println("Account deleted!");
		} catch (SQLException e)
		{
			System.out.println("Error! Check data!");
		}
	}
	
	public static void updateAccount(Account a) 
	{
		try {
			String statement="UPDATE Account SET type=?, amount=?, dateofCreation=?, clientID=? where accountID=?";
			PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
			prepSt.setString(1, a.getType());
			prepSt.setFloat(2, a.getAmount());
			prepSt.setString(3, a.getDateofCreation());
			prepSt.setString(4, a.getClientID());
			int succes = prepSt.executeUpdate();
			
			if(succes == 1)
				System.out.println("Account Updated!");
			} catch (SQLException e)
			{
				System.out.println("Error! Check data!");
			}
			
		}
	
	public static void viewAccounts() {
		try { 
			PreparedStatement pst = DBConnection.getConnection().prepareStatement("Select * FROM Account");
			ResultSet rs = pst.executeQuery();
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			while(rs.next()) {
			System.out.printf("|AccountID: %s | Type: %s | Amount: %s | Date of creation: %s | ClientID: %s |\n", rs.getString("accountID"), rs.getString("type"), rs.getFloat("amount"), rs.getString("dateofCreation"), rs.getString("clientID"));
			}

		} catch (SQLException e) {
			System.out.println("Error! Check data!");
		}
	}

}

