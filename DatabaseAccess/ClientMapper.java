package DatabaseAccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import BusinessLogic.Client;

public class ClientMapper {
	
	public static void insertClient(Client c) throws SQLException {
		String statement= "INSERT INTO client (clientID, name, IDnumber, CNP, address, loginID) VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement prepSt = DBConnection.getConnection().prepareStatement(statement);
		prepSt.setString(1, c.getClientID());
		prepSt.setString(2, c.getName());
		prepSt.setString(3, c.getIDnumber());
		prepSt.setString(4, c.getCNP());
		prepSt.setString(5, c.getAddress());
		prepSt.setString(6, c.getLoginID());
		
		int succes= prepSt.executeUpdate();
		if(succes == 1)
			System.out.println("Client inserted!");
	}
	
	public Client getClientById(String clientID) throws SQLException {
		Client foundClient= new Client();
		String sqlQuery="SELECT clientID, name, IDnumber, CNP, address, loginID FROM Client";
		PreparedStatement prepSt= DBConnection.getConnection().prepareStatement(sqlQuery);
		prepSt.setString(100, clientID);
		ResultSet queryResult=prepSt.executeQuery();
		
		while (queryResult.next()) {
			String id=queryResult.getString("clientID");
			String Nume=queryResult.getString("name");
			String IDnumber=queryResult.getString("IDnumber");
			String cNP=queryResult.getString("CNP");
			String address=queryResult.getString("address");
			String loginID=queryResult.getString("loginID");
			
			foundClient.setClientID(id);
			foundClient.setName(Nume);
			foundClient.setIDnumber(IDnumber);
			foundClient.setCNP(cNP);
			foundClient.setAddress(address);
			foundClient.setLoginID(loginID);
			
			System.out.println("Client ID: "+id+", Nume: "+Nume+", ID number: "+IDnumber+", CNP:"+cNP+", Address: "+address+", loginID:"+loginID);
		}
		return foundClient;
	}
	
	public static void deleteClient(String idToDelete) throws SQLException{
		
		
		try {
		String statement= "DELETE from Client where clientID=?";
		PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
		prepSt.setString(1, idToDelete);


		int succes = prepSt.executeUpdate();
		
		if(succes == 1)
			System.out.println("Client deleted!");
		} catch (SQLException e)
		{
			System.out.println("Error! Check data!");
		}
	}
	
	public static void updateClient(Client c) 
	{
		try {
			String statement="UPDATE Client SET name=?, idNumber=?, CNP=?, address=?, loginID=? where clientID=?";
			PreparedStatement prepSt=DBConnection.getConnection().prepareStatement(statement);
			prepSt.setString(1, c.getName());
			prepSt.setString(2, c.getIDnumber());
			prepSt.setString(3, c.getCNP());
			prepSt.setString(4, c.getAddress());
			prepSt.setString(5, c.getLoginID());
			int succes = prepSt.executeUpdate();
			
			if(succes == 1)
				System.out.println("Client Updated!");
			} catch (SQLException e)
			{
				System.out.println("Error! Check data!");
			}
			
		}
	
	public static void viewClients() {
		try { 
			PreparedStatement pst = DBConnection.getConnection().prepareStatement("Select * FROM Client");
			ResultSet rs = pst.executeQuery();
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			while(rs.next()) {
			System.out.printf("|ClientID: %s | Name: %s | IDnumber: %s | CNP: %s | Address: %s | LoginID: %s |\n", rs.getString("clientID"), rs.getString("name"), rs.getString("IDnumber"), rs.getString("CNP"), rs.getString("address"), rs.getString("loginID"));
			}

		} catch (SQLException e) {
			System.out.println("Error! Check data!");
		}
	}

}
