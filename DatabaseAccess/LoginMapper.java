package DatabaseAccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import BusinessLogic.Login;

public class LoginMapper {
	
	public static void CreateAccount(Login l) throws SQLException {
		try {
		String statement= "Insert into Login(loginID, username, password, role) values (?, ?, ?, ?)";
		PreparedStatement prepST=DBConnection.getConnection().prepareStatement(statement);
		prepST.setString(1, l.getLoginID());
		prepST.setString(2,  l.getUsername());
		prepST.setString(3, l.getPassword());
		prepST.setBoolean(4, l.isRole());
		
		if ( l.isRole() == true )
			System.out.println("Client account created!");
		if ( l.isRole() == false )
			System.out.println("Admin account created!");
		
		int succes = prepST.executeUpdate();
		
		if(succes == 1)
			System.out.println("Account created!");
		} catch (SQLException e)
		{
			System.out.println("Error! Check data!");
		}
	}
	
	public static void MakeLogin(Login l) throws SQLException{
		try {
			PreparedStatement pst=DBConnection.getConnection().prepareStatement("Select username, password, role from Login");
			ResultSet rs=pst.executeQuery();
			while ( rs.next()) {
				String username=rs.getString("username");
				String password=rs.getString("password");
				boolean role=rs.getBoolean("role");
				System.out.println("|Username: %s "+username+"| Password: %s "+password+"| Role: %s |"+role);
			}
				
		} catch (SQLException e)
		{
			System.out.println("Error! Check data!");
		}
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter username: ");
		String username=sc.next();
		System.out.println("Enter password: ");
		String password=sc.next();

	}

}


